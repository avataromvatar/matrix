/**
 * Matrix.hpp
 *
 *  Created on: 21 окт. 2019 г.
 *      Author: avatar
 *
 * Перегруженные операторы + - * / передают ссылку на обьект который был создан с помощью new
 * в процессе работы оператора. Для удаления этого временного обьекта вызовите либо присвоение =
 * либо конструктор копирование. Если у обьекта указатель tmp != 0 то это временный обьект
 * и он будет удален после использования
 *
 * @attention Такие конструкции как TMatrix<A> + TMatrix<A> Запрещены!!!
 * используйте либо TMatrix<A> = TMatrix<A> + TMatrix<A> + .... TMatrix<A>n
 * либо TMatrix<A>(TMatrix<A> + TMatrix<A>) иначе гарантирована утечка памяти
 */

#ifndef CORE_MATRIX_MATRIX_HPP_
#define CORE_MATRIX_MATRIX_HPP_

#define TMATRIX_RANDOM_GENERATOR
#define TMATRIX_STATIC_ASSERT
#define PRINTF_TMATRIX

#include "stdint.h"

#ifdef PRINTF_TMATRIX
#include "stdio.h"
#endif

#ifdef TMATRIX_STATIC_ASSERT
#include "type_traits"
#endif

#ifdef USE_EXCEPTIONS
#include "error_exception.h"
#endif

#ifdef TMATRIX_RANDOM_GENERATOR
#include <random>
#endif

//#include <iostream>

namespace matrix
{
// один из раб вариантов проверки что пихают в typename и создания специлизаций
//  template<class T, class Enable = void>
//  class TMatrix;
//
//  template<typename A>
//    class TMatrix<A,typename std::enable_if<std::is_arithmetic<A>::value >::type>

  template<typename A>
    class TMatrix
    {
      public:
#ifdef TMATRIX_STATIC_ASSERT
	static_assert(std::is_arithmetic<A>::value, "TMatrix<A> A - must be is_arithmetic");
#endif
	enum eTMatrix : uint8_t
	{
	  ROW, COLUMN
	};

	/**
	 * Конструктор копирования
	 * @param copy_from из какой матрицы создать наш класс
	 */
	TMatrix (const TMatrix<A> &copy_from)
	{
	  m = copy_from.m;
	  n = copy_from.n;

	  M = new A*[m];
	  for (uint32_t i = 0; i < m; i++)
	  {
	    M[i] = new A[n];
	    for (uint32_t i1 = 0; i1 < n; i1++)
	      M[i][i1] = copy_from.M[i][i1];
	  }

	  //log ("copy constructor");

	}
	/**
	 * Конструктор перемещения
	 * @param copy_from из какой матрицы создать наш класс
	 */
	TMatrix (TMatrix<A> &&other)
	{
	  m = other.m;
	  n = other.n;

	  M = other.M;
	  other.M = nullptr;

	  //log ("move constructor");

	}

	/**
	 * Конструктор с произвольным размерами
	 * @param m строки
	 * @param n столбцы
	 */
	TMatrix (uint32_t m, uint32_t n)
	{

	  this->m = m;
	  this->n = n;
	  M = new A*[m];
	  for (uint32_t i = 0; i < m; i++)
	    M[i] = new A[n];
//	log ("class constructor with size m and n");
	}

#ifdef TMATRIX_RANDOM_GENERATOR
	/**
	 * Конструктор с произвольным размерами
	 * @param m строки
	 * @param n столбцы
	 */
	TMatrix (uint32_t m, uint32_t n, int min, int max)
	{
	  std::random_device rd;   // non-deterministic generator
	  std::random_device rd1;   // non-deterministic generator
	  std::random_device rd2;   // non-deterministic generator
	  std::random_device rd3;   // non-deterministic generator
	  std::mt19937 gen1 (rd () * rd1 ());  // to seed mersenne twister.
	  std::mt19937 gen2 (54668421874);  // to seed mersenne twister.
	  std::mt19937 gen3 (rd () * rd2 ());  // to seed mersenne twister.
	  std::mt19937 gen (rd3 ());  // to seed mersenne twister.
	  std::uniform_int_distribution<> dist1 (0, 3); // distribute results between 1 and 6 inclusive.
	  std::uniform_int_distribution<> dist (min, max); // distribute results between 1 and 6 inclusive.
	  this->m = m;
	  this->n = n;
	  M = new A*[m];
	  for (uint32_t i = 0; i < m; i++)
	  {
	    M[i] = new A[n];
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      if (dist1 (gen) == 0)
		M[i][i1] = dist (gen1);
	      else if (dist1 (gen) == 1)
		M[i][i1] = dist (gen2);
	      else if (dist1 (gen) == 2)
		M[i][i1] = dist (gen3);
	      else if (dist1 (gen) == 3)
		M[i][i1] = dist (gen);
	      else
		M[i][i1] = dist (gen1);
	    }
	  }
	//  log ("class constructor with random fill and size m and n");
	}
#endif

	virtual ~TMatrix ()
	{

	  destroy ();
	//  log (" class destructor");
	}

	/**
	 * NoSafe быстрый доступ к элементам матрицы
	 * @param index номер строки/столбца
	 * @return
	 */
	A* operator [] (uint32_t const &index)
	{
	  return M[index];
	}

	/**
	 * Доступ к элементу с проверкой размеров
	 * @param m
	 * @param n
	 * @return
	 */
	A& operator () (uint32_t m, uint32_t n)
	{
#ifdef USE_EXCEPTIONS
	if (this->m <= m || this->n <= n)
	  error_exceptions("TMatrix m or n error size");
#endif
	  return M[m][n];
	}

	/**
	 * Копирование матриц
	 * @param copy_from
	 */
	void operator () (const TMatrix<A> &copy_from)
	{

#ifdef USE_EXCEPTIONS
	if (this->m != copy_from.getLen (ROW) || this->n != copy_from.getLen (COLUMN))
	  error_exceptions("TMatrix cope error - different len");
#endif
	//  log ("copy class");
	  *this = copy_from;
	}

	/**
	 * Опиратор присвоения копированием
	 * @param right
	 * @return
	 */
	TMatrix<A>& operator = (const TMatrix<A> &copy_from)
	{
	  //проверка на самоприсваивание
	  if (this == &copy_from)
	  {
	    return *this;
	  }
	//  log ("copy assignment operator");
	  destroy ();
	  m = copy_from.m;
	  n = copy_from.n;

	  M = new A*[m];
	  for (uint32_t i = 0; i < m; i++)
	  {
	    M[i] = new A[n];
	    for (uint32_t i1 = 0; i1 < n; i1++)
	      M[i][i1] = copy_from.M[i][i1];
	  }

	  return *this;
	}

	/**
	 * Присвоение перемещением
	 * @param other
	 * @return
	 */
	TMatrix<A>& operator= (TMatrix<A> &&other)
	{
	  //log ("move assignment operator");
	  if (this == &other)
	    return *this;
	  destroy ();
	  m = other.m;
	  n = other.n;
	  M = other.M;
	  other.M = nullptr;
	  return *this;
	}

	/**
	 * Присвоение значения
	 * @param val
	 * @return
	 */
	TMatrix<A>& operator= (const A &val)
	{
	 // log ("Присвоение значения");

	  for (uint32_t i = 0; i < m; i++)
	  {

	    for (uint32_t i1 = 0; i1 < n; i1++)
	      M[i][i1] = val;
	  }

	  return *this;
	}

	/**
	 * Возвращает либо кол-во строк либо столбцов
	 * @param mn Указать что вернуть
	 * @return
	 */
	uint32_t getLen (eTMatrix mn)
	{
	  if (mn == ROW)
	    return m;
	  else
	    return n;
	}

	/**
	 * Возвращает кол0во строк
	 * @return
	 */
	uint32_t getRowLen ()
	{
	  return m;
	}

	/**
	 * Возвращает кол-во столбцов
	 * @return
	 */
	uint32_t getCollumsLen ()
	{
	  return n;
	}

	TMatrix<A> operator + (const TMatrix<A> &right)
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '+' different len");
#endif
	 // printf ("una + : %d + %d\n", this, &right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] + right.M[i][i1];
	    }
	  }
	  return tmp;
	}
	TMatrix<A> operator + (const A &right)
	{

	//  printf ("una + val : %d + %d\n", this, (int) right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] + right;
	    }
	  }
	  return tmp;
	}

	TMatrix<A> operator - (const TMatrix<A> &right)
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '-' different len");
#endif
	 // printf ("una - : %d + %d\n", this, &right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] - right.M[i][i1];
	    }
	  }
	  return tmp;
	}
	TMatrix<A> operator - (const A &right)
	{

	//  printf ("una - val : %d + %d\n", this, (int) right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] - right;
	    }
	  }
	  return tmp;
	}

	TMatrix<A> operator * (const TMatrix<A> &right)
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '+' different len");
#endif
	 // printf ("una * : %d + %d\n", this, &right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] * right.M[i][i1];
	    }
	  }
	  return tmp;
	}

	TMatrix<A> operator * (const A &right)
	{

	//  printf ("una * val : %d + %d\n", this, (int) right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] * right;
	    }
	  }
	  return tmp;
	}

	TMatrix<A> operator / (const TMatrix<A> &right)
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '+' different len");
#endif
//	  printf ("una / : %d + %d\n", this, &right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] / right.M[i][i1];
	    }
	  }
	  return tmp;
	}

	TMatrix<A> operator / (const A &right)
	{

//	  printf ("una / val : %d + %d\n", this, (int) right);
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = M[i][i1] / right;
	    }
	  }
	  return tmp;
	}
// В качестве внешнего оператора + может использоватся (хоть он внутри класса но он в не его)
//	 friend const TMatrix<A> operator +(const TMatrix<A> &left,
//			const TMatrix<A> &right)
//	{
//#ifdef USE_EXCEPTIONS
//		if (left.m != right.m || left.n != right.n)
//			error_exceptions("TMatrix operator '+' different len");
//#endif
//		printf("bin + : %d + %d\n",&left,&right);
//		TMatrix<A> tmp(left.m, left.n);
//
//		for (uint32_t i = 0; i < left.m; i++)
//		{
//			for (uint32_t i1 = 0; i1 < left.n; i1++)
//			{
//				(tmp)[i][i1] = left.M[i][i1] + right.M[i][i1];
//			}
//		}
//		return tmp;
//	}

	TMatrix<A>& operator += (const TMatrix<A> &right) //унарный оператор
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '+=' different len");
#endif
//	  printf ("una += : %d + %d\n", this, &right);
	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      M[i][i1] = M[i][i1] + right.M[i][i1];
	    }
	  }

	  return *this;
	}

	TMatrix<A>& operator -= (const TMatrix<A> &right) //унарный оператор
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '-=' different len");
#endif

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      M[i][i1] = M[i][i1] - right.M[i][i1];
	    }
	  }

	  return *this;
	}

	TMatrix<A>& operator *= (const TMatrix<A> &right) //унарный оператор
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '*=' different len");
#endif

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      M[i][i1] = M[i][i1] * right.M[i][i1];
	    }
	  }

	  return *this;
	}
	TMatrix<A>& operator /= (const TMatrix<A> &right) //унарный оператор
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '/=' different len");
#endif

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      M[i][i1] = M[i][i1] / right.M[i][i1];
	    }
	  }

	  return *this;
	}

	TMatrix<A>& operator - () //унарный оператор
	{

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      M[i][i1] = -1 * M[i][i1];
	    }
	  }
	  return *this;
	}
	TMatrix<A>& operator ++ () //префиксное прибавление
	{

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      M[i][i1]++;
	    }
	  }
	  return *this;
	}
	TMatrix<A> operator ++ (int r) //постфиксное прибавление
	{
	  TMatrix<A> tmp (*this);
	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      M[i][i1]++;
	    }
	  }
	  return tmp;
	}

//	friend  TMatrix<A> operator -(const TMatrix<A> &left,
//			const TMatrix<A> &right) //Для внешней реализации бинарного оператора
//	{
//#ifdef USE_EXCEPTIONS
//		if (left.m != right.m || left.n != right.n)
//			error_exceptions("TMatrix operator '-' different len");
//#endif
//
//		TMatrix<A> tmp(left.m, left.n);
//
//		for (uint32_t i = 0; i < left.m; i++)
//		{
//			for (uint32_t i1 = 0; i1 < left.n; i1++)
//			{
//				(tmp)[i][i1] = left.M[i][i1] - right.M[i][i1];
//			}
//		}
//		return tmp;
//	}

//	friend  TMatrix<A> operator *(const TMatrix<A> &left,
//			const TMatrix<A> &right) //Для внешней реализации бинарного оператора
//	{
//#ifdef USE_EXCEPTIONS
//		if (left.m != right.m || left.n != right.n)
//			error_exceptions("TMatrix operator '*' different len");
//#endif
//
//		TMatrix<A> tmp(left.m, left.n);
//
//		for (uint32_t i = 0; i < left.m; i++)
//		{
//			for (uint32_t i1 = 0; i1 < left.n; i1++)
//			{
//				(tmp)[i][i1] = left.M[i][i1] - right.M[i][i1];
//			}
//		}
//		return tmp;
//	}
//
//	friend TMatrix<A> operator /(const TMatrix<A> &left,
//			const TMatrix<A> &right)
//	{
//#ifdef USE_EXCEPTIONS
//		if (left.m != right.m || left.n != right.n)
//			error_exceptions("TMatrix operator '/' different len");
//#endif
//
//		TMatrix<A> tmp(left.m, left.n);
//
//		for (uint32_t i = 0; i < left.m; i++)
//		{
//			for (uint32_t i1 = 0; i1 < left.n; i1++)
//			{
//				(tmp)[i][i1] = left.M[i][i1] - right.M[i][i1];
//			}
//		}
//		return tmp;
//	}

	/**
	 * Поэлементное или
	 * @param right
	 * @return
	 */
	TMatrix<A> operator | (const TMatrix<A> &right) //,TMatrix<A> &right
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '|' different len");
#endif
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (tmp)[i][i1] = (int) M[i][i1] | (int) right.M[i][i1];
	    }
	  }

	  return *tmp;
	}

	/**
	 * Поэлементное и
	 * @param right
	 * @return
	 */
	TMatrix<A> operator & (const TMatrix<A> &right) //,TMatrix<A> &right
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '&' different len");
#endif
	  TMatrix<A> tmp (m, n);

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      (*tmp)[i][i1] = (int) M[i][i1] & (int) right.M[i][i1];
	    }
	  }

	  return tmp;
	}

	/**
	 * Сравнение матриц
	 * @param right
	 * @return
	 */
	bool operator == (const TMatrix<A> &right) //,TMatrix<A> &right
	{
#ifdef USE_EXCEPTIONS
	if (m != right.m || n != right.n)
	  error_exceptions("TMatrix operator '==' different len");
#endif

	  for (uint32_t i = 0; i < m; i++)
	  {
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      if (M[i][i1] != right.M[i][i1])
		return false;
	    }
	  }
	  return true;
	}
#ifdef PRINTF_TMATRIX
	void printMatrix ()
	{
	  printf ("%-8d | ", 0);
	  for (uint32_t i = 0; i < m; i++)
	  {
	    printf ("%-8d | ", i);
	  }
	  printf ("\n");
	  for (uint32_t i = 0; i < m; i++)
	  {
	    printf ("%-8d | ", i);
	    for (uint32_t i1 = 0; i1 < n; i1++)
	    {
	      if (std::is_floating_point<A>::value)
	      {
		printf ("%-8.3f | ", M[i][i1]);
	      }
	      else
		printf ("%-8d | ", M[i][i1]);
	    }
	    printf ("\n");
	  }

	}
	void log (const char *str)
	{
	  printf ("[%d] %s \n", this, str);
	}
#endif
      private:
	TMatrix ()
	{
//	  log ("empty class constructor ");
	}

	void destroy ()
	{
	  if (M != 0 || M != nullptr)
	  {
	    for (uint32_t i = 0; i < m; i++)
	    {
	      delete[] M[i];
	    }
	    delete[] M;
	  }

	}

	A **M = nullptr;
	uint32_t m = 0;
	uint32_t n = 0;
	TMatrix<A> *tmp = nullptr;
    };

//	  /**
//		 * Поэлементное сложение матриц
//		 * @param right
//		 * @return
//		 */
//
//		 const TMatrix<float>& operator +(const TMatrix<float> &left, const TMatrix<float> &right) //,TMatrix<A> &right
//		{
//	#ifdef USE_EXCEPTIONS
//			if (left.m != right.m || left.n != right.n)
//				error_exceptions("TMatrix operator '+' different len");
//	#endif
//			TMatrix<float> tmp(left.m,left.n);
//
////			tmp->tmp = tmp;
//			//std::shared_ptr<TMatrix<A>*> sptr(tmp);
//
//			for (uint32_t i = 0; i < left.m; i++)
//			{
//				for (uint32_t i1 = 0; i1 < left.n; i1++)
//				{
//					(tmp)[i][i1] = left.M[i][i1] + right.M[i][i1];
//				}
//			}
////			if(right.tmp)
////			    delete &right;
////			if(left.tmp)
////			    delete left;
//			return TMatrix<float>(tmp);
//		}

  typedef TMatrix<float> Matrix;
  typedef TMatrix<bool> BoolMatrix;
  typedef TMatrix<int> IntMatrix;

} /* namespace Matrix */

#endif /* CORE_MATRIX_MATRIX_HPP_ */
